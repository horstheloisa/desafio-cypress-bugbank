Feature: Login de Usuário

Background: Acessar tela login
  Given que acesso a aplicação
    And cadastro um novo usuário com os seguintes valores: 
|      campo      |       valor      |
|      email      |teste12@teste.com.br|
|       nome      |       teste      |
|      senha      |       teste      |
|confirmacao senha|       teste      |
        
    ############### Login ###############
    Scenario: Logar com sucesso 
        And na tela de login preencho o campo "email" com "teste12@teste.com.br" 
        And na tela de login preencho o campo "senha" com "teste" 
        And clico no botão "Acessar"
    Then a tela principal é aberta "teste"

    Scenario: Logar com dados incorretos
        And na tela de login preencho o campo "email" com "teste1232@teste.com.br"
        And na tela de login preencho o campo "senha" com "teste34"
        And clico no botão "Acessar"
    Then é apresentada a mensagem de usuário ou senha inválidos

    ############### Máscaras de senhas ###############
    Scenario: Validar que a senha é oculta ao usuário
            And na tela de login preencho o campo "senha" com "teste"
    Then é ocultada a senha ao usuário

    Scenario: Validar que a senha é exibida ao usuário
        And na tela de login preencho o campo "senha" com "teste" 
        And clico no ícone para exibir a senha do usuário na tela de login
    Then é exibida a senha ao usuário "teste"

    ############### Campos obrigatórios ###############
    
    Scenario: Logar sem informar a senha 
        And na tela de login preencho o campo "email" com "teste12@teste.com.br"
        And clico no botão "Acessar"
    Then é apresentada a mensagem de senha obrigatória

    Scenario: Logar sem informar o email 
        And na tela de login preencho o campo "senha" com "teste"
        And clico no botão "Acessar"
    Then é apresentada a mensagem de e-mail obrigatório

    ############### Campo e-mail com formatos inválidos ###############
    Scenario Outline: Logar com formato de e-mail inválido
        And na tela de login preencho o campo "email" com "<emailInvalido>"
    Then é apresentada a mensagem de e-mail com formato inválido
          
    Examples:
        | emailInvalido | 
        | teste@teste  | 
        | teste@  | 
        | teste.com.br  | 
        | @teste.com.br  | 


