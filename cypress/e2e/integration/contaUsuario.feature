Feature: Conta de Usuário

Background: Acessar tela login
     And cadastro um novo usuário com os seguintes valores: 
|      campo      |         valor        |
|      email      |usuario01@teste.com.br|
|       nome      |       usuario01      |
|      senha      |       usuario01      |
|confirmacao senha|       usuario01      |

    And logo com o novo usuário:
|campo|         valor        |
|email|usuario01@teste.com.br|
|senha|       usuario01      |
| nome|       usuario01      |
    And cadastro um novo usuário com os seguintes valores: 
|      campo      |         valor        |
|      email      |usuario02@teste.com.br|
|       nome      |       usuario02      |
|      senha      |       usuario02      |
|confirmacao senha|       usuario02      |
    And logo com o novo usuário:
|campo|         valor        |
|email|usuario02@teste.com.br|
|senha|       usuario02      |
| nome|       usuario02      |

Scenario: Validar saldo na abertura da conta 
        And saldo é de "1.000"
        And é apresentado no extrato "1" registro, sendo um registro no valor de "1.000" com o texto "Abertura de conta" e descrição "Saldo adicionado ao abrir conta"

Scenario: Realizar uma transfêrencia para uma conta existente e com valor menor que o saldo
        And realizo uma transferência no valor de "500" para "usuario01"
    Then é apresentada a mensagem "Transferencia realizada com sucesso"
        And saldo é de "500"
        And é apresentado no extrato "2" registro, sendo um registro no valor de "-R$ 500" com o texto "Transferência enviada" e descrição "-"

Scenario: Realizar uma transfêrencia para uma conta existente e com valor maior que o saldo
        And realizo uma transferência no valor de "5000" para "usuario01"
    Then é apresentada a mensagem "Você não tem saldo suficiente para essa transação"
        And saldo é de "500"  
        And é apresentado no extrato "2" registro, sendo um registro no valor de "-R$ 500" com o texto "Transferência enviada" e descrição "-"

Scenario: Realizar uma transfêrencia para uma conta existente e com valor 0 
        And realizo uma transferência no valor de "0" para "usuario01"
    Then é apresentada a mensagem "Valor da transferência não pode ser 0 ou negativo"
        And saldo é de "500" 
        And é apresentado no extrato "2" registro, sendo um registro no valor de "-R$ 500" com o texto "Transferência enviada" e descrição "-"
   

