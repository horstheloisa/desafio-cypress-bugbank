Feature: Cadastro de Usuários

    Background: Acessar tela login
        When clico no botão Registrar

    ############### Criação de usuários ###############

    Scenario: Criar um novo usuário com sucesso
            And preencho o campo "email" com "teste@teste23.com.br"
            And preencho o campo "nome" com "Teste Cypress"
            And preencho o campo "senha" com "testeCy@"
            And preencho o campo "confirmacao senha" com "testeCy@"
            And clico no botão "Cadastrar"
        Then a conta é criada com sucesso
            And fecho o modal
            
    Scenario: Criar um novo usuário com senha de confirmação incorreta   
            And preencho o campo "email" com "teste@teste23.com.br"
            And preencho o campo "nome" com "Teste Cypress"
            And preencho o campo "senha" com "testeCy@"
            And preencho o campo "confirmacao senha" com "naoehamesma@"
            And clico no botão "Cadastrar"
        Then a conta não é criada com sucesso
            And fecho o modal

    ############### Máscaras de senhas ###############

    Scenario: Validar senhas ocultas
            And preencho o campo "senha" com "teste"
        Then a senha é ocultada ao usuário
            And preencho o campo "confirmacao senha" com "teste@"                
        Then a confirmação da senha é ocultada ao usuário    

    Scenario: Validar que as senhas são exibidas ao usuário
            And preencho o campo "senha" com "teste"
            And preencho o campo "confirmacao senha" com "teste@"
            And clico no ícone para exibir a senha do usuário
        Then a senha é exibida ao usuário "teste"
            And clico no ícone para exibir a confirmação da senha do usuário            
        Then a confirmação da senha é exibida ao usuário "teste@"                 

    ############### Campos obrigatórios ###############

    Scenario: Criar um novo usuário sem informar o e-mail   
            And preencho o campo "nome" com "Teste Cypress"
            And preencho o campo "senha" com "testeCy@"
            And preencho o campo "confirmacao senha" com "naoehamesma@"
            And clico no botão "Cadastrar"
        Then a mensagem de e-mail obrigatório é apresentada

    Scenario: Criar um novo usuário sem informar a senha    
            And preencho o campo "email" com "teste@teste23.com.br"
            And preencho o campo "nome" com "Teste Cypress"
            And clico no botão "Cadastrar"
        Then a mensagem de senha obrigatória é apresentada

    Scenario: Criar um novo usuário sem informar a confirmação de senha    
            And preencho o campo "email" com "teste@teste23.com.br"
            And preencho o campo "nome" com "Teste Cypress"
            And preencho o campo "senha" com "teste@"
            And clico no botão "Cadastrar"
        Then a mensagem de confirmação de senha obrigatória é apresentada
    

    ############### Campo e-mail com formatos inválidos ###############
    Scenario Outline: Criar um novo usuário com formato de e-mail inválido
            And preencho o campo "email" com "<emailInvalido>"
        Then a mensagem de e-mail com formato inválido é apresentada
          
    Examples:
        | emailInvalido | 
        | teste@teste  | 
        | teste@  | 
        | teste.com.br  | 
        | @teste.com.br  | 



       

    