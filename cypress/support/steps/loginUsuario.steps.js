import { loginUsuarioElements } from '../elements/loginUsuario.elements';
import { cadastroUsuarioPage } from '../pages/cadastroUsuario.page';
import { loginUsuarioPage } from '../pages/loginUsuario.page';


////// GIVEN ////// 
Given('acesso a aplicação', () => {
	loginUsuarioPage.acessarAplicacao();
})

////// AND ////// 
And('cadastro um novo usuário com os seguintes valores:', (tabela) => {
	cadastroUsuarioPage.cadastrarUsuario(tabela);	
});

And('na tela de login preencho o campo {string} com {string}', (campo, valor) => {	
	loginUsuarioPage.preencherCampo(campo, valor);
});

And('clico no botão "Acessar"', () => {	
	loginUsuarioPage.clicarBotaoAcessar();
})

And('fecho o modal', () => {
	loginUsuarioPage.fecharBotaoModal();
})

And('é apresentada a mensagem de e-mail com formato inválido', () => {
	loginUsuarioPage.validarMensagemEmailFormatoInvalidoLogin();
})

And('clico no ícone para exibir a senha do usuário na tela de login', () => {
	loginUsuarioPage.clicarIconeExibirSenhaLogin();
})

And('na tela de login preencho o campo {string} com {string}', (campo, valor) => {

	const elementosCampos = {
		email: loginUsuarioElements.campoEmailLogin(),
		senha: loginUsuarioElements.campoSenhaLogin(),
	};
		
	const campoSelecionado = elementosCampos[campo];
	cadastroUsuarioPage.preencherCampo(campoSelecionado, valor);

});

////// THEN  ////// 

Then('a tela principal é aberta {string}', (nomeUsuario) => {
	loginUsuarioPage.validarTelaPrincipalAberta(nomeUsuario);
})

Then('é apresentada a mensagem de e-mail obrigatório', () => {
	loginUsuarioPage.validarMensagemEmailObrigatorioLogin();
})

Then('é apresentada a mensagem de senha obrigatória', () => {
	loginUsuarioPage.validarMensagemSenhaObrigatoriaLogin();
})

Then('é exibida a senha ao usuário {string}', (senha) => {
	loginUsuarioPage.validarIconeSenhaLogin(senha);
})

Then('é ocultada a senha ao usuário', () => {
	loginUsuarioPage.validarSenhaOcultadaLogin();
})

Then('é apresentada a mensagem de usuário ou senha inválidos', () => {
	loginUsuarioPage.validarUsuarioSenhaInvalidosLogin();
})

