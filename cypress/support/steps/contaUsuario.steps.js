import { contaUsuarioElements } from "../elements/contaUsuario.elements";
import { contaUsuarioPages } from "../pages/contaUsuario.pages";
import { cadastroUsuarioPage } from "../pages/cadastroUsuario.page";

////// GIVEN ////// 
Given('acesso a aplicação', () => {
	loginUsuarioPage.acessarAplicacao();
})

////// AND ////// 
And('cadastro um novo usuário com os seguintes valores:', (tabela) => {
	cadastroUsuarioPage.cadastrarUsuario(tabela);
});

And('saldo é de {string}', (valor) => {
    contaUsuarioPages.validarSaldo(valor);
})

And('logo com o novo usuário:', (tabela) => {
    contaUsuarioPages.logar(tabela);
})

And('realizo uma transferência no valor de {string} para {string}', (valor, usuario) => { 
    contaUsuarioPages.realizarTransferencia(valor, usuario);
})

And('a transferência é realizada com {string}', (mensagem) => {
    contaUsuarioPages.validarTransferencia(mensagem);
})

And('é apresentado no extrato {string} registro, sendo um registro no valor de {string} com o texto {string} e descrição {string}', (quantidade, valor, texto, descricao) => {
    contaUsuarioPages.validarExtrato(quantidade, valor, texto, descricao);
})

////// THEN ////// 
Then('é apresentada a mensagem {string}', (mensagem) => {
    contaUsuarioPages.validarMensagem(mensagem);
})

