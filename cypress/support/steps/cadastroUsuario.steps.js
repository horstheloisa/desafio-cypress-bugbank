import { cadastroUsuarioElements } from '../elements/cadastroUsuario.elements';
import {cadastroUsuarioPage} from '../pages/cadastroUsuario.page'

////// GIVEN ////// 
Given('que acesso a aplicação', () => {
	cadastroUsuarioPage.acessarAplicacao();
});

////// AND ////// 
And('preencho o campo {string} com {string}', (campo, valor) => {

	const elementosCampos = {
		email: cadastroUsuarioElements.campoEmailCriarUsuario(),
		nome: cadastroUsuarioElements.campoNomeCriarUsuario(),
		senha: cadastroUsuarioElements.campoSenhaCriarUsuario(),
		"confirmacao senha": cadastroUsuarioElements.campoConfirmacaoSenhaCriarUsuario(),
	};
		
	const campoSelecionado = elementosCampos[campo];
	cadastroUsuarioPage.preencherCampo(campoSelecionado, valor);

});

And('clico no botão "Cadastrar"', () => {	
	cadastroUsuarioPage.clicarBotaoCadastrarNovoUsuario();
})

And('fecho o modal', () => {
	cadastroUsuarioPage.fecharBotaoModal();
})

And('a mensagem de e-mail com formato inválido é apresentada', () => {
	cadastroUsuarioPage.validarMensagemEmailFormatoInvalido();
})

And('clico no ícone para exibir a senha do usuário', () => {
	cadastroUsuarioPage.clicarIconeExibirSenha();
})

And('clico no ícone para exibir a confirmação da senha do usuário', () => {
	cadastroUsuarioPage.clicarIconeExibirConfirmacaoSenha();
})

////// WHEN ////// 
When('clico no botão Registrar', () => {
	cadastroUsuarioPage.clicarBotaoRegistrar();
});

////// THEN ////// 
Then('a conta é criada com sucesso', () => {
	cadastroUsuarioPage.validarMensagemContaCadastradoComSucesso();
})

Then('a conta não é criada com sucesso', () => {
	cadastroUsuarioPage.validarMensagemAsSenhasNaoSaoIguais();
})

Then('a mensagem de e-mail obrigatório é apresentada', () => {
	cadastroUsuarioPage.validarMensagemEmailObrigatorio();
})

Then('a mensagem de senha obrigatória é apresentada', () => {
	cadastroUsuarioPage.validarMensagemSenhaObrigatoria();
})

Then('a mensagem de confirmação de senha obrigatória é apresentada', () => {
	cadastroUsuarioPage.validarMensagemConfirmacaoSenhaObrigatoria();
})

Then('a senha é exibida ao usuário {string}', (senha) => {
	cadastroUsuarioPage.validarIconeSenha(senha);
})

Then('a confirmação da senha é exibida ao usuário {string}', (senha) => {
	cadastroUsuarioPage.validarIconeConfirmacaoSenha(senha);
})

Then('a senha é ocultada ao usuário', () => {
	cadastroUsuarioPage.validarSenhaOcultada();
})

Then('a confirmação da senha é ocultada ao usuário', () => {
	cadastroUsuarioPage.validarConfirmacaoSenhaOcultada();
})

