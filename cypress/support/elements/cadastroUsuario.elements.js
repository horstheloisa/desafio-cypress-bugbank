class CadastroUsuarioElements{

    botaoRegistrar = () => '.style__ContainerButton-sc-1wsixal-0.ihdmxA.button__child'
    campoEmailCriarUsuario = () => '.input__default[name="email"]:eq(1)'
    campoNomeCriarUsuario = () => 'input[name="name"]'
    campoSenhaCriarUsuario = () => 'input[name="password"]:eq(1)'
    campoConfirmacaoSenhaCriarUsuario = () => 'input[name="passwordConfirmation"]'
    botaoCriarContaComSaldo = () => '#toggleAddBalance'
    botaoCadastrarUsuario = () => '.style__ContainerButton-sc-1wsixal-0.CMabB.button__child'    
    mensagemModal = () => '#modalText'
    mensagemFormatoInvalido = () => '.kOeYBn > .input__warging'
    mensagemEmailObrigatorio = () => '.style__ContainerFieldInput-sc-s3e9ea-0.kOeYBn.input__child > .input__warging'
    mensagemSenhaObrigatoria = () => 'p.input__warging:eq(4)'
    mensagemConfirmacaoSenhaObrigatoria = () => 'p.input__warging:eq(5)'
    iconeExibirOcultarSenha = () => '.login__eye:eq(1)'
    iconeExibirOcultarConfirmacaoSenha = () => '.login__eye:eq(2)'
    botaoFecharModal = () => '.styles__Button-sc-8zteav-5.gyHUvN'
    botaoSair = () => '#btnExit'
}

export const cadastroUsuarioElements = new CadastroUsuarioElements();
