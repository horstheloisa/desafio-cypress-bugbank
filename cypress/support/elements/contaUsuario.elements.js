class ContaUsuarioElements{

    saldoConta = () => 'p#textBalance > span'
    botaoRegistrar = () => '.style__ContainerButton-sc-1wsixal-0.ihdmxA.button__child'
    campoEmailCriarUsuario = () => '.input__default[name="email"]:eq(1)'
    campoNomeCriarUsuario = () => 'input[name="name"]'
    campoSenhaCriarUsuario = () => 'input[name="password"]:eq(1)'
    botaoCriarContaComSaldo = () => '#toggleAddBalance'
    botaoCadastrarUsuario = () => '.style__ContainerButton-sc-1wsixal-0.CMabB.button__child'   
    mensagemModal = () => '#modalText'
    botaoFecharModal = () => '#btnCloseModal'
    botaoVoltar = () => '#btnBack'
    botaoExtrato = () => '#btn-EXTRATO'
    botaoTransferencia = () => '#btn-TRANSFERÊNCIA'
    campoNumeroConta = () => 'input[type=accountNumber]'
    campoDigitoConta = () => 'input[type=digit]'
    campoValorTransferencia = () => 'input[type=transferValue]'
    campoDescricaoTransferencia = () => 'input[type=description]'
    botaoTransferirAgora = () => '.style__ContainerButton-sc-1wsixal-0.CMabB.button__child'
    valorTransferenciaExtrato = () => '#textTransferValue'
    textoTransferenciaExtrato = () => '#textTypeTransaction'
    descricaoTransferenciaExtrato = () => '#textDescription'
    dataTransferenciaExtrato = () => '#textDateTransaction'
    saldoDisponivelExtrato = () => '#textBalanceAvailable'
    quantidadeRegistrosExtrato = () => '.bank-statement__Transaction-sc-7n8vh8-13.fUCxBP'

}

export const contaUsuarioElements = new ContaUsuarioElements();