class LoginUsuarioElements{

    campoEmailLogin = () => '.style__ContainerFormLogin-sc-1wbjw6k-0 > :nth-child(1) > .input__default'
    campoSenhaLogin = () => '.style__ContainerFormLogin-sc-1wbjw6k-0 > .login__password > .style__ContainerFieldInput-sc-s3e9ea-0 > .input__default'
    botaoAcessarUsuario = () => '.style__ContainerButton-sc-1wsixal-0.otUnI.button__child'    
    mensagemModal = () => '#modalText'
    mensagemFormatoInvalidoLogin = () => '.style__ContainerFieldInput-sc-s3e9ea-0.kOeYBn.input__child > .input__warging'
    mensagemEmailObrigatorioLogin = () => '.style__ContainerFieldInput-sc-s3e9ea-0.kOeYBn.input__child > .input__warging'
    mensagemSenhaObrigatoriaLogin = () => '.login__password > .style__ContainerFieldInput-sc-s3e9ea-0.kOeYBn.input__child > .input__warging'
    iconeExibirOcultarSenhaLogin = () => '.login__password > .login__eye:eq(0)'
    botaoFecharModalLogin = () => '.styles__Button-sc-8zteav-5.gyHUvN'
    telaPincipalAberta = () => '.home__ContainerText-sc-1auj767-7.iDA-Ddb'
    
}

export const loginUsuarioElements = new LoginUsuarioElements();
