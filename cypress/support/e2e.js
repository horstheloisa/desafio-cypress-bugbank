import './commands'
import 'cypress-v10-preserve-cookie'
import '@shelex/cypress-allure-plugin';


beforeEach(() => {

  cy.visit('https://bugbank.netlify.app/'); 
  cy.restoreLocalStorage();

});

afterEach(() => {
  cy.saveLocalStorage();
});

