/// <reference types="Cypress" />

import { loginUsuarioElements } from '../elements/loginUsuario.elements'

class LoginUsuarioPage {

    clicarBotaoAcessar(){
        cy.get(loginUsuarioElements.botaoAcessarUsuario())
        .should('be.visible')
        .click()
    }       
    
    preencherCampo(campo, valor){   
        const elementosCampos = {
            email: loginUsuarioElements.campoEmailLogin(),
            senha: loginUsuarioElements.campoSenhaLogin(),
        };
            
        const campoSelecionado = elementosCampos[campo];

        cy.get(campoSelecionado)
        .should(($campoSelecionado) => {
            expect($campoSelecionado).to.have.length(1)})
        .type(valor, {force: true})
    }        
  
    validarMensagemContaCadastradoComSucesso(){
        cy.get(loginUsuarioElements.mensagemModal())
        .should('be.visible')
        .contains('foi criada com sucesso')
    }

    validarMensagemAsSenhasNaoSaoIguais(){
        cy.get(loginUsuarioElements.mensagemModal())
        .should('be.visible')
        .should('have.text','As senhas não são iguais.\n')
    }

    validarMensagemErroEmailCadastrado(){
        cy.get(loginUsuarioElements.mensagemErroEmailCadastro())
        .should('be.visible')
    }

    fecharBotaoModal(){
        cy.get(loginUsuarioElements.botaoFecharModal())
        .should(($campo) => {
            expect($campo).to.have.length(1)})
        .click({force: true})
    }

    validarTelaPrincipalAberta(nomeUsuario){
        cy.get(loginUsuarioElements.telaPincipalAberta())
        .should('have.text', 'Olá ' + nomeUsuario + ',bem vindo ao BugBank :)')
    }

    validarMensagemEmailFormatoInvalidoLogin(){
        cy.get(loginUsuarioElements.mensagemFormatoInvalidoLogin())
        .should('have.text', 'Formato inválido')
    }

    validarMensagemEmailObrigatorioLogin(){
        cy.get(loginUsuarioElements.mensagemEmailObrigatorioLogin())
        .should('have.text', 'É campo obrigatório')
    }

    validarMensagemSenhaObrigatoriaLogin(){
        cy.get(loginUsuarioElements.mensagemSenhaObrigatoriaLogin())
        .should('have.text', 'É campo obrigatório')
    }

    loginUsuarioElements(){
        cy.get(loginUsuarioElements.iconeExibirOcultarSenhaLogin())
        .click({force: true});
    }

    validarIconeSenhaLogin(senha){        
        cy.get(loginUsuarioElements.campoSenhaLogin())
        .should('have.value', senha)  
        .should('have.attr', 'type').and('equal', 'text')               
    }

    validarSenhaOcultadaLogin(){
        cy.get(loginUsuarioElements.campoSenhaLogin())
        .should('have.attr', 'type').and('equal', 'password')     
    }

    validarUsuarioSenhaInvalidosLogin(){
        cy.get(loginUsuarioElements.mensagemModal())
        .should('have.text', 'Usuário ou senha inválido.\nTente novamente ou verifique suas informações!')     
    }

    clicarIconeExibirSenhaLogin(){
        cy.get(loginUsuarioElements.iconeExibirOcultarSenhaLogin())
        .click({force: true});
    }
}

export const loginUsuarioPage = new LoginUsuarioPage();