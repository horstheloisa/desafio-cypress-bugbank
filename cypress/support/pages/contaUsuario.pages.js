import { contaUsuarioElements } from "../elements/contaUsuario.elements";
import { cadastroUsuarioElements } from "../elements/cadastroUsuario.elements";
import { loginUsuarioElements } from "../elements/loginUsuario.elements";
import { loginUsuarioPage } from "../pages/loginUsuario.page";
import { cadastroUsuarioPage } from "./cadastroUsuario.page";

class ContaUsuarioPages {

    acessarAplicacao(){
        cy.visit('https://bugbank.netlify.app/')
    }

    cadastrarUsuario(tabela){
        cy.restoreLocalStorage();

        cadastroUsuarioPage.clicarBotaoRegistrar();  

        const matriz = tabela.rawTable.slice(1);

        for (let i = 0; i < matriz.length; i++) {
            const linha = matriz[i];
            const campo = linha[0];
            const valor = linha[1];

            const elementosCampos = {
                email: cadastroUsuarioElements.campoEmailCriarUsuario(),
                nome: cadastroUsuarioElements.campoNomeCriarUsuario(),
                senha: cadastroUsuarioElements.campoSenhaCriarUsuario(),
                "confirmacao senha": cadastroUsuarioElements.campoConfirmacaoSenhaCriarUsuario(),
            };
                
            const campoSelecionado = elementosCampos[campo];
            cadastroUsuarioPage.preencherCampo(campoSelecionado, valor)   

            cy.task('log', matriz[i])
        }

        cadastroUsuarioPage.clicarBotaoContaComSaldo();
        cadastroUsuarioPage.clicarBotaoCadastrarNovoUsuario();
        cadastroUsuarioPage.validarMensagemContaCadastradoComSucesso();

     
        cy.get(cadastroUsuarioElements.mensagemModal).invoke('text').then((texto) => {        

            let numeroConta = texto; 

            
            cy.log(numeroConta.split(':')[1].split('-')[0])
            cy.log(numeroConta.split(':')[1].split('-')[1])

            cy.wrap(numeroConta.split(':')[1].split(' ')[1].split('-')[0]).as('numeroConta')
            cy.wrap(numeroConta.split(':')[1].split('-')[1]).as('numeroConta')
        })

        cy.getAllLocalStorage();
        cadastroUsuarioPage.fecharBotaoModal();
    }

    validarSaldo(valor){
        cy.get(contaUsuarioElements.saldoConta())
        .should('contain.text', valor)
    }

    logar(tabela){

            let nomeUsuario;
            const matriz = tabela.rawTable.slice(1);
    
            for (let i = 0; i < matriz.length; i++) {
                const linha = matriz[i];
                const campo = linha[0];
                const valor = linha[1];
    
                const elementosCampos = {
                    email: loginUsuarioElements.campoEmailLogin(),
                    senha: loginUsuarioElements.campoSenhaLogin(),
                };
                    
                const campoSelecionado = elementosCampos[campo];

                if (campo === 'nome'){
                    nomeUsuario = valor;
                } else {
                    cadastroUsuarioPage.preencherCampo(campoSelecionado, valor)   
                }
                
                cy.getMinhaLista().then(lista => {

                    const usuarioEncontrado = lista.find(item => item.nomeUsuario === nomeUsuario)    
                    
                    if (usuarioEncontrado) {
                        cy.task('log', 'usuario logado:' + JSON.stringify(usuarioEncontrado))
                    } else { 
                        cy.task('log', 'usuário não foi encontrado, portanto, será adicionado na lista')
                    }
                });
            }          

            loginUsuarioPage.clicarBotaoAcessar();
            loginUsuarioPage.validarTelaPrincipalAberta(nomeUsuario)            
    }

    realizarTransferencia(valor, usuario){

        cy.get(contaUsuarioElements.botaoTransferencia())
        .click()

        cy.getMinhaLista().then((lista) => {   
       
            const usuarioEncontrado = lista.find(item => item.nomeUsuario === usuario)

            const numeroConta = usuarioEncontrado.numeroConta;
            const digitoConta = usuarioEncontrado.digitoConta;
          
            if (numeroConta !== null) {
                cy.get(contaUsuarioElements.campoNumeroConta())
                  .should('be.visible')
                  .type(numeroConta);
            }
          
            if (digitoConta !== null) {
                cy.get(contaUsuarioElements.campoDigitoConta())
                  .should('be.visible')
                  .type(digitoConta);
            }
        });   
        
        cy.get(contaUsuarioElements.campoValorTransferencia())
        .should('be.visible')
        .type(valor)

        cy.get(contaUsuarioElements.botaoTransferirAgora())
        .click({force: true})
    }

    validarMensagem(mensagem){
        cy.get(contaUsuarioElements.mensagemModal())
        .should('be.visible', {force:true})
        .should('have.text', mensagem)

        cy.get(contaUsuarioElements.botaoFecharModal())
        .click();

        debugger;
        cy.get(contaUsuarioElements.botaoVoltar())
        .click();  

        const telaTransferencia = Cypress.$(contaUsuarioElements.botaoVoltar());

        cy.task('log', telaTransferencia.length)
        if (telaTransferencia.length > 1) {    
            cy.get(contaUsuarioElements.botaoVoltar()).click();        
        }
    }

    validarExtrato(quantidade, valor, texto, descricao){   
        cy.get(contaUsuarioElements.botaoExtrato()).should('be.visible').click();
        cy.get(contaUsuarioElements.quantidadeRegistrosExtrato()).should('have.length', quantidade)
        cy.contains(contaUsuarioElements.textoTransferenciaExtrato(), texto).should('be.visible');
        cy.contains(contaUsuarioElements.descricaoTransferenciaExtrato(), descricao).should('be.visible');
        cy.contains(contaUsuarioElements.valorTransferenciaExtrato(), valor).should('be.visible');
    }
}

export const contaUsuarioPages = new ContaUsuarioPages();