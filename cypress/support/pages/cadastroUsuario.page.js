/// <reference types="Cypress" />

import {cadastroUsuarioElements} from '../elements/cadastroUsuario.elements'
import { contaUsuarioElements } from '../elements/loginUsuario.elements';


class CadastroUsuarioPage {

    acessarAplicacao(){
        cy.visit('https://bugbank.netlify.app/')
    }
      
    cadastrarUsuario(tabela){
        this.verificarSeEstaLogado();

        const matriz = tabela.rawTable.slice(1);
        let nomeUsuario;

        cy.getMinhaLista().then(lista => {
            if (!(lista.find(item => item.nomeUsuario === 'usuario01' && lista.find(item => item.nomeUsuario === 'usuario02')))) {
                
                this.clicarBotaoRegistrar();  

                for (let i = 0; i < matriz.length; i++) {
                    const campo = matriz[i][0];
                    const valor = matriz[i][1];
        
                    const elementosCampos = {
                        email: cadastroUsuarioElements.campoEmailCriarUsuario(),
                        nome: cadastroUsuarioElements.campoNomeCriarUsuario(),
                        senha: cadastroUsuarioElements.campoSenhaCriarUsuario(),
                        "confirmacao senha": cadastroUsuarioElements.campoConfirmacaoSenhaCriarUsuario(),
                    };
                        
                    const campoSelecionado = elementosCampos[campo];
                    this.preencherCampo(campoSelecionado, valor)   
                }
        
                this.clicarBotaoContaComSaldo();
                this.clicarBotaoCadastrarNovoUsuario();     
                this.validarMensagemContaCadastradoComSucesso();
        
                cy.get(cadastroUsuarioElements.mensagemModal()).invoke('text').then(mensagemObtida => {
                    const regex = / (\d+)-(\d+) /;  //expressão regular para definir um padrão de correspondência em uma string, no caso para pegar número da conta + digíto 
                    const match = mensagemObtida.match(regex); //irá procurar na mensagem obtida se existe a correspondência declarada na variável regex, e irá guardar três valores, o valor original 123-4, o valor 123 e o valor 4 
          
                    if (match) {
                        const numeroConta = match[1]; // aqui irá pegar o segundo valor do array match, no caso 123
                        const digitoConta = match[2]; // aqui irá pegar o terceiro valor do array match, no caso 4
                     
                        for (let i = 0; i < matriz.length; i++) {
                            const campo = matriz[i][0];
                            const valor = matriz[i][1];
        
                            if (campo === 'nome'){                        
                                nomeUsuario = valor;
                                cy.task('log', nomeUsuario)
                            }                        
                        }
                    
                        cy.task('log', 'usuário: '+ nomeUsuario, 'numero conta: ' + numeroConta, 'digitoConta: ' + digitoConta)    
                        cy.adicionarNaLista(nomeUsuario, numeroConta, digitoConta);                                                 
                    }   
                })               
                this.fecharBotaoModal();
            }
        });           
    };   

    verificarSeEstaLogado() {

        cy.preserveCookieOnce('bugbank-auth')
        const estaLogado = Cypress.$(cadastroUsuarioElements.botaoSair());

        if (estaLogado.length > 0) {    
            cy.get(cadastroUsuarioElements.botaoSair())
                                          .click();
            } else {
                cy.task('log', 'Não está logado')
            }
        }    
      
    clicarBotaoContaComSaldo(){
        cy.get(cadastroUsuarioElements.botaoCriarContaComSaldo())
        .click({force: true})
    }

    clicarBotaoRegistrar(){
        cy.get(cadastroUsuarioElements.botaoRegistrar())
        .should('be.visible')
        .click()
    }       
    
    preencherCampo(campo, valor){   
        cy.get(campo)
        .should(($campo) => {
            expect($campo).to.have.length(1)})
        .type(valor, {force: true})
    }     
    
  
    clicarBotaoCadastrarNovoUsuario(){
        cy.get(cadastroUsuarioElements.botaoCadastrarUsuario())
        .should(($campo) => {
            expect($campo).to.have.length(1)})
        .click({force: true})
    }

    validarMensagemContaCadastradoComSucesso(){
        cy.get(cadastroUsuarioElements.mensagemModal())
        .should('be.visible')
        .contains('foi criada com sucesso')
    }

    validarMensagemAsSenhasNaoSaoIguais(){
        cy.get(cadastroUsuarioElements.mensagemModal())
        .should('be.visible')
        .should('have.text','As senhas não são iguais.\n')
    }

    validarMensagemErroEmailCadastrado(){
        cy.get(cadastroUsuarioElements.mensagemErroEmailCadastro())
        .should('be.visible')
    }

    fecharBotaoModal(){
        cy.get(cadastroUsuarioElements.botaoFecharModal())
        .should(($campo) => {
            expect($campo).to.have.length(1)})
        .click({force: true})
    }

    validarMensagemEmailFormatoInvalido(){
        cy.get(cadastroUsuarioElements.mensagemFormatoInvalido())
        .should('have.text', 'Formato inválido')
    }

    validarMensagemEmailObrigatorio(){
        cy.get(cadastroUsuarioElements.mensagemEmailObrigatorio())
        .should('have.text', 'É campo obrigatório')
    }

    validarMensagemSenhaObrigatoria(){
        cy.get(cadastroUsuarioElements.mensagemSenhaObrigatoria())
        .should('have.text', 'É campo obrigatório')
    }

    validarMensagemConfirmacaoSenhaObrigatoria(){
        cy.get(cadastroUsuarioElements.mensagemConfirmacaoSenhaObrigatoria())
        .should('have.text', 'É campo obrigatório')
    }

    clicarIconeExibirSenha(){
        cy.get(cadastroUsuarioElements.iconeExibirOcultarSenha())
        .click({force: true});
    }

    clicarIconeExibirConfirmacaoSenha(){
        cy.get(cadastroUsuarioElements.iconeExibirOcultarConfirmacaoSenha())
        .click({force: true});
    }

    validarIconeSenha(senha){        
        cy.get(cadastroUsuarioElements.campoSenhaCriarUsuario())
        .should('have.value', senha)  
        .should('have.attr', 'type').and('equal', 'text')               
    }

    validarIconeConfirmacaoSenha(senha){
        cy.get(cadastroUsuarioElements.campoConfirmacaoSenhaCriarUsuario())
        .should('have.value', senha)  
        .should('have.attr', 'type').and('equal', 'text')     
    }

    validarSenhaOcultada(){
        cy.get(cadastroUsuarioElements.campoSenhaCriarUsuario())
        .should('have.attr', 'type').and('equal', 'password')     
    }

    validarConfirmacaoSenhaOcultada(){
        cy.get(cadastroUsuarioElements.campoConfirmacaoSenhaCriarUsuario())
        .should('have.attr', 'type').and('equal', 'password')     
    }
}

export const cadastroUsuarioPage = new CadastroUsuarioPage();