// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

import 'cypress-localstorage-commands';

let usuariosCadastrados = [];
let LOCAL_STORAGE_MEMORY = {};

Cypress.Commands.add('adicionarNaLista', (nomeUsuario, numeroConta, digitoConta) => {
    return new Promise((resolve) => {
      usuariosCadastrados.push({
        nomeUsuario: nomeUsuario,
        numeroConta: numeroConta,
        digitoConta: digitoConta
      });
      resolve(usuariosCadastrados);
    });
  });
  
Cypress.Commands.add('getMinhaLista', () => {
    return new Promise((resolve) => {
        resolve(usuariosCadastrados);
      });
});

Cypress.Commands.add("saveLocalStorage", () => {
  Object.keys(localStorage).forEach(key => {
    LOCAL_STORAGE_MEMORY[key] = localStorage[key];
  });
});

Cypress.Commands.add("restoreLocalStorage", () => {
  Object.keys(LOCAL_STORAGE_MEMORY).forEach(key => {
    localStorage.setItem(key, LOCAL_STORAGE_MEMORY[key]);
  });
});
