const cucumber = require('cypress-cucumber-preprocessor').default;
const allureWriter = require('@shelex/cypress-allure-plugin/writer');

module.exports = (on, config) => {
  on('file:preprocessor', cucumber());

  on('task', {
    log(message) {
      console.log(message);
      return null;
    }
  });

  allureWriter(on, config); 
  return config;
};


/// <tipos de referência=”@shelex/cypress-allure-plugin” />





